# React Coding Challenge 2

An online shopping similar to Amazon, Lazada, or Shopee.

## Prerequisite

- Must be knowledgeable in Javascript and [React](https://reactjs.org/).
- Must be familiar with client side routing such as [React Router](https://reacttraining.com/react-router/web) or similar.
- Should know how to connect frontend and backend application through AJAX, GraphQL, Web Socket or etc.

## Technical Specifications

- It must be a [Single Page Application](https://flaviocopes.com/single-page-application/) written using ReactJS. You could start by using [Create React App](https://create-react-app.dev/).
- Use any backend technologies you prefer.
- Feel free to use any CSS frameworks.
- [Code split](https://reactjs.org/docs/code-splitting.html) your routes. `(1pts)`
- Manage your state using [Context API](https://reactjs.org/docs/context.html), or with state management libraries like [Redux](https://redux.js.org/), [Mobx](https://mobx.js.org/README.html), or other similar tools. `(1pts)`
- Implement [Error Boundaries](https://reactjs.org/docs/error-boundaries.html). `(1pts)`

## Functional Specifications

##### As a guest:
- I can sign up using my name, address, contact number, email, and password. `(1pts)`
  - must be a valid email. `(1pts)`
  - must be an 11 digit contact number. `(1pts)`
  - name, address, contact number and password cannot be blank. `(1pts)`

##### As a registered user:
- I can sign in using email, and password. `(1pts)`
- I can add products to sell them. `(1pts)`
  - A product could contain the following fields: name, image, price, and category.
  - For simplicity, copy image address from [Unsplash](https://unsplash.com/). No need to implement a file upload.
- I can edit my products. `(1pts)`
- I can remove my products. `(1pts)`
- I can see all of my products. `(1pts)`
  - I should have a counter for the number of times it was purchased. `(1pts)`
- I can see all the transactions / orders that I made along with the products associate with it. `(2pts)`
  - It must display transction / order number and the date of the transaction, ordered by the most recent purchase I made. `(1pts)`

##### As a guest / registered user:
- I can search a product that I want to purchase. `(1pts)`
  - Implement pagination to avoid loading all the products at once. `(3pts)` An example would be at the bottom of the page you could do the following:
    - Put a previous button, page numbers and next button.
    - Add a load more button.
    - Whenever the users reaches near the bottom of the page, it will load up the next products.
- I can add to cart a product along with the quantity. `(1pts)`
- On my cart, I can remove a product. `(1pts)`
- On my cart, I can increase and decrease the no. of quantity of a product. `(1pts)`
- On my cart, I can check/uncheck the product that I want to checkout. `(1pts)`
- On my cart, I can checkout the selected products and be directed to the checkout page. `(1pts)`
- On my checkout page:
  - If I'm a guest, add fields for: name, address, and contact number. `(1pts)`
  - If I'm a registered user, I should see my address and can make changes to it. `(1pts)`
  - Group the selected products by sellers. `(1pts)`
  - Display the product name, price, quantity, and total amount of each item. `(1pts)`
  - Display the total amount that I must pay. `(1pts)`
- From the checkout page, I can place an order. `(1pts)`
  - Placing an order will create a transaction and must generate a transaction/order number from the backend. `(1pts)`
  - After placing an order, remove the selected products from the cart. `(1pts)`
- I can immediately purchase a product and be directed to the checkout page. `(1pts)`

## Deadline

- Prepare to defend your solution. ⚔🛡

> A total of 34 points.

Good Luck 😁
<br />
Happy Coding
