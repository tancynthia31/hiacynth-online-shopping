import React from 'react';
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export function deleteItem(itemId: string) {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Users").doc(user.uid)
            .collection("Cart")
            .doc(itemId)
            .delete()
            .then(() => {
                alert('successfully deleted');
            })
            .catch(error => {
                alert(error.message);
            });
    }
}

export function checkItem(id: any, checked: any) {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Users").doc(user.uid)
            .collection('Cart').doc(id)
            .update({
                checked: Boolean(!checked)
            }).then(() => {
                console.log('ok');
            }).catch(error => {
                alert(error.message);
            });
    }
}

export function addItem(id: any, qty: number, price: number) {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Users").doc(user.uid)
            .collection('Cart').doc(id)
            .update({
                quantity: qty + 1,
                amount: price * (qty + 1)
            }).then(() => {
                console.log('ok');
            }).catch(error => {
                alert(error.message);
            });
    }
}

export function decreaseItem(id: any, qty: number, price: number) {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Users").doc(user.uid)
            .collection('Cart').doc(id)
            .update({
                quantity: qty === 1 ? 1 : qty -= 1,
                amount: qty === 1 ? price : (price * (qty -= 1))
            }).then(() => {
                console.log('ok');
            }).catch(error => {
                alert(error.message);
            });
    }
}

export function orderItems(selectedItems:any, userData:any, amount:number) {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        let customerOrders: string[] = [];
        selectedItems.forEach((item: any) => {
          const ord = item.productId + "/" +
           item.quantity + "/" + item.productName + "/" +
            item.price + "/" + item.amount;
          customerOrders.push(ord);
        })
        const orderData = {
          orderNumber: new Date().valueOf(),
          firstName: userData.firstName,
          lastName: userData.lastName,
          totalAmount: Number(amount),
          address: userData.address,
          contact: userData.contact,
          dateOrdered: firebase.firestore.Timestamp.now(),
          orders: customerOrders,
          userId: user.uid,
        }
        db.collection("Orders").doc()
          .set(orderData).then(() => {
            console.log('ok');
          }).catch(error => {
            alert(error.message);
          });
        selectedItems.forEach((item: any) => {
          db.collection("Products")
            .doc(item.productId)
            .update({
              purchasedNum: Number(item.purchasedNum) 
              + Number(item.quantity)
            })
            .then(() => {
              console.log("ok");
            })
        })
        selectedItems.forEach((item: any) => {
          db.collection("Users")
            .doc(user.uid)
            .collection("Cart")
            .doc(item.id)
            .delete()
            .then(() => {
             console.log("successfully ordered")
            }).then(() => {
              console.log("ok");
            })
        })
      }
}