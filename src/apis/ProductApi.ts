import React from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export function addToCart(productDetails: any, productId: string, qty: number) {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Users").doc(user.uid)
            .collection('Cart')
            .where('productId', '==', productId).get().then(snapshot => {
                if (snapshot.empty) {
                    db.collection("Users").doc(user.uid)
                        .collection('Cart').doc()
                        .set({
                            productId: productId,
                            productName: productDetails.productName,
                            category: productDetails.category,
                            productImage: productDetails.productImage,
                            price: Number(productDetails.price),
                            amount: Number(productDetails.price * qty),
                            quantity: Number(qty),
                            checked: Boolean(true),
                            purchasedNum: Number(productDetails.purchasedNum),
                            sellerName: productDetails.sellerName,
                        })
                        .then(() => {
                            console.log('ok');
                        })
                        .catch(error => {
                            alert(error.message);
                        });
                } else {
                    alert("product already added to cart");
                }
            })
    }
}

export function editProduct(productId: string, productDetails:any) { 
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Products").doc(productId).update({
                productName: productDetails.productName,
                price: productDetails.price,
                category: productDetails.category,
                productImage: productDetails.productImage,
            }).then(() => {
              console.log("successfully edited");
            })
                .catch(error => {
                    alert(error.message);
                });
        }
}

export function deleteItem(productId: string, productDetails:any) { 
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Products")
          .doc(productId)
          .delete()
          .then(() => {
            console.log('successfully deleted');
        })
        .catch(error => {
            alert(error.message);
        });
    }
}

export function addItem(data:any, userData:any) { 
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        const newData = {
            sellerName: userData.firstName + " " + userData.lastName,
            sellerUserId: user.uid,
            productName: data.productName,
            price: Number(data.price),
            category: data.category,
            productImage: data.productImage,
            purchasedNum: 0,
            dateCreated: firebase.firestore.Timestamp.now()
        }
        db.collection("Products").doc()
        .set(newData).then(() => {
        console.log("Successfully added");
        }).catch(error => {
            alert(error.message);
        });
    }
}