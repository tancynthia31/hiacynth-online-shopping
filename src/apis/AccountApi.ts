import React from 'react';
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export function signOut() {
    firebase.auth().signOut().then(function () {
        console.log("Logged out successfully");
    }).catch(error => {
        alert(error.message);
    });
}

export function signIn(email: string, password: string) {
    firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(res => {
            console.log(res);
            window.location.href = 'http://localhost:3000/account'
        })
        .catch(error => {
            alert(error.message);
        });
}

export function registerAccount(data: any) {
    const db = firebase.firestore();
    firebase
        .auth()
        .createUserWithEmailAndPassword(data.email, data.password)
        .then((userCredential: firebase.auth.UserCredential) => {
            db.collection("Users")
                .doc(userCredential.user!.uid)
                .set({
                    firstName: data.firstName,
                    lastName: data.lastName,
                    email: data.email,
                    contact: data.contact,
                    address: data.address,
                    password: data.password
                })
                .then(() => {
                    console.log('Successfully registered');
                })
                .catch(error => {
                    alert(error.message);
                });
        })
}

export function changeAddress(currentAdd:string) {
    const user = firebase.auth().currentUser;
    const db = firebase.firestore();
    if (user) {
        db.collection("Users").doc(user.uid)
        .update({
            address: currentAdd,
        }).then(() => {
          console.log("Successfully updated");
        }).catch(error => {
            alert(error.message);
        });
     }
}
