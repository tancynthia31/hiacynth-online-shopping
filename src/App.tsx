import React,
{ FunctionComponent }
  from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch
} from "react-router-dom";
import { AuthProvider } from "./Contexts/AuthContext";
import { CartProvider } from "./Contexts/CartContext";
import { ProductProvider } from "./Contexts/ProductContext";
import { UserProvider } from "./Contexts/UserContext";
import { SellerProductProvider } from "./Contexts/SellerProductContext";
import { OrdersProvider } from "./Contexts/OrdersContext";
import AppRoute from './Routes/AppRoute';
import ErrorBoundary from "./Components/ErrorBoundary"

const App: FunctionComponent = () => {
  return (
    <Router>
      <ErrorBoundary>
        <AuthProvider>
          <Switch>
            <UserProvider>
              <ProductProvider>
                <SellerProductProvider>
                  <CartProvider>
                    <OrdersProvider>
                      < AppRoute />
                    </OrdersProvider>
                  </CartProvider>
                </SellerProductProvider>
              </ProductProvider>
            </UserProvider>
          </Switch>
        </AuthProvider>
      </ErrorBoundary>
    </Router>
  );
}

export default App;
