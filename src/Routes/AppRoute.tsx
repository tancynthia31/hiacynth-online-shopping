import React, { FunctionComponent } from 'react'
import {Route} from "react-router-dom";
import NotFoundPage from '../Pages/PageNotFound';
import Login from '../Pages/Login';
import Registration from '../Pages/Registration';
import Account from '../Pages/Account';
import Selling from '../Pages/Selling';
import Homepage from '../Pages/Homepage';
import Cart from '../Pages/Cart';
import Checkout from '../Pages/Checkout';
import Transactions from '../Pages/Transactions';
import PrivateRoute from '../Components/AppPrivateRoute'

const RouteContexts: FunctionComponent = () => {
    return (
        <div>
            <PrivateRoute path="/account" exact component={Account}></PrivateRoute>
            <PrivateRoute path="/selling" exact component={Selling}></PrivateRoute>
            <PrivateRoute path="/cart" exact component={Cart}></PrivateRoute>
            <PrivateRoute path="/checkout" exact component={Checkout}></PrivateRoute>
            <PrivateRoute path="/transactions" exact component={Transactions}></PrivateRoute>
            <Route path="/login" exact component={Login}></Route>
            <Route path="/signup" exact component={Registration}></Route>
            <Route path="/" exact component={Homepage}></Route>
            <Route path="/not-found-page" exact component={NotFoundPage}></Route>
        </div>
    )
}

export default RouteContexts