import React,
{
    useEffect,
    useState,
    createContext
} from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export const UserContext = createContext<any>({});

export const UserProvider = ({ children }: any) => {
    const user = firebase.auth().currentUser;
    const [userData, setUserData] = useState<any>([]);
    const db = firebase.firestore();

    useEffect(() => {
        const fetchData = () => {
            if (user) {
                db.collection("Users").doc(user.uid)
                    .onSnapshot(snapshot => {
                        setUserData(snapshot.data());
                    }, (error) => {
                        alert(error.message);
                    });
            }
        }
        fetchData();
    }, []);

    return (
        <UserContext.Provider value={[userData, setUserData]}>
            {children}
        </UserContext.Provider>
    )
}
