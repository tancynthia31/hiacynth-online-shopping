import React, 
{
useEffect,
useState,
createContext
} from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export const CartContext = createContext<any>({});

export const CartProvider = ({ children }: any) => {
  const user = firebase.auth().currentUser;
  const db = firebase.firestore();
  const [cartItems, setCartItems] = useState<any>([]);
  const [amount, setAmount] = useState<number>();
  const [selectedItems, setSelectedItems] = useState<any>([]);
  const [group, setGroup] = useState<any>([]);

  useEffect(() => {
    const fetchData = () => {
      if (user) {
        db.collection("Users").doc(user.uid)
          .collection('Cart')
          .onSnapshot(snapshot => {
            const listItems = snapshot.docs.map(doc => ({
              id: doc.id,
              productId: doc.data().productId,
              quantity: doc.data().quantity,
              productName: doc.data().productName,
              price: doc.data().price,
              amount: doc.data().amount,
              checked: Boolean(doc.data().checked),
              sellerName: doc.data().sellerName,
              purchasedNum: doc.data().purchasedNum
            }))
            setCartItems(listItems)
            let amounts = 0
            let allSelected: string[] = [];
            listItems.forEach((item: any) => {
              if (item.checked === true) {
                amounts += Number(item.amount)
                allSelected.push(item)
              }
            });
            setAmount(Number(amounts))
            setSelectedItems(allSelected)

            const groups = allSelected.reduce((groups:any, item:any) => {
              const seller = item.sellerName
              if (!groups[seller]) {
                groups[seller] = [];
              }
              groups[seller].push({
                "seller": seller,
                "items" : item
              });
              return groups;
            }, {});
            console.log(groups);
            setGroup(groups);
          })
      }


    };
    fetchData();

  }, []);


  return (
    <CartContext.Provider
      value={{
        items: [cartItems, setCartItems],
        total: { amount },
        selected: [selectedItems, setSelectedItems],
        groupItems: [group, setGroup]
      }}>
      {children}
    </CartContext.Provider>
  )
}
