import React, 
{
useEffect,
useState,
createContext
} from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export const ProductContext = createContext<any>({});

export const ProductProvider = ({children}:any) => {
    const [productItems, setProductItems] =  useState<any>([]);
    const [search, setSearch] = useState("");
    const [filteredItems, setFilteredItems] = useState<any>([]);
    const db = firebase.firestore();

  useEffect(() => {
    const fetchData = () => {
        db.collection("Products")
          .onSnapshot(snapshot => {
            const listItems = snapshot.docs.map(doc => ({
              id: doc.id,
              productName: doc.data().productName,
              category: doc.data().category,
              productImage: doc.data().productImage,
              price: doc.data().price,
              purchasedNum: Number(doc.data().purchasedNum)
            }))
            setProductItems(listItems)
          })
      }
    fetchData();
  }, []);

  useEffect(() => {
    setFilteredItems(
      productItems.filter((item:any) => {
        return item.productName.toLowerCase().includes(search.toLowerCase())
      })
    );
  }, [search, productItems]);

    return (
        <ProductContext.Provider 
        value={{
          products: [productItems, setProductItems],
          filtered: [filteredItems, setFilteredItems],
          searchItems: [search, setSearch]
          }}>
            {children}
        </ProductContext.Provider>
    )
}
