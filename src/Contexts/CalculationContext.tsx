import React, 
{
useEffect,
useState,
createContext
} from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export const CalculationContext = createContext<any>({});

export const CalculationProvider = ({ children }: any) => {
  const user = firebase.auth().currentUser;
  const db = firebase.firestore();
  const [cartItems, setCartItems] = useState<any>([]);
  const [amount, setAmount] = useState(0);

  useEffect(() => {
    const fetchData = () => {
      if (user) {
        db.collection("Users").doc(user.uid)
          .collection('Cart')
          .onSnapshot(snapshot => {
            const listItems = snapshot.docs.map(doc => ({
              id: doc.id,
              productId: doc.data().productId,
              quantity: doc.data().quantity,
              productName: doc.data().productName,
              price: doc.data().price,
              amount: doc.data().amount,
              checked: Boolean(doc.data().checked)
            }))
            setCartItems(listItems)
          })
      }
        var count = 0;
        cartItems.forEach((item:any) => {
         if(item.checked === true){
             count += Number(item.amount)
         }
        })
        setAmount(count)
    };
    fetchData();
  }, []);


  return (
    <CalculationContext.Provider value={amount}>
      {children}
    </CalculationContext.Provider>
  )
}
