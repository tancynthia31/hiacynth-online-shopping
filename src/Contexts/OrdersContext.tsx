import React, 
{
useEffect,
useState,
createContext
} from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export const OrdersContext = createContext<any>({});

export const OrdersProvider = ({ children }: any) => {
  const user = firebase.auth().currentUser;
  const db = firebase.firestore();
  const [orders, setOrders] = useState<any>([]);
  
  useEffect(() => {
    const fetchData = () => {
      if (user) {
        db.collection("Orders")
        .where('userId', '==', user.uid)
        .orderBy('dateOrdered', 'desc')
          .onSnapshot(snapshot => {
            const listItems = snapshot.docs.map(doc => ({
              id: doc.id,
              orderNumber: doc.data().orderNumber,
              dateOrdered: doc.data().dateOrdered.toDate().toString(),
              orders: doc.data().orders,
              totalAmount: doc.data().totalAmount
            }))
            setOrders(listItems)
          })
    
      }
    };
    fetchData();
     
  }, []);

  return (
    <OrdersContext.Provider value={[orders, setOrders]}>
      {children}
    </OrdersContext.Provider>
  )
}
