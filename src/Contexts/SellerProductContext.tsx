import React, 
{
useEffect,
useState,
createContext
} from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";

export const SellerProductContext = createContext<any>({});

export const SellerProductProvider = ({ children }: any) => {
  const user = firebase.auth().currentUser;
  const db = firebase.firestore();
  const [sellerItems, setSellerItems] = useState<any>([]);
  const [productDetails, setProductDetails] = useState<any>([]);
  const [categories] = useState(
    [
      "Select category",
      "Mens Fashion",
      "Women's Fashion",
      "Toys Kids and Babies",
      "Food and Beverages",
      "Mobile and Gadgets",
      "Home Entertainment",
      "Laptops and Computers",
      "Home Appliances",
      "Health and Personal Care",
      "Collectibles",
      "Shoes and Sandals",
    ]);

  useEffect(() => {
    const fetchData = () => {
      if (user) {
        db.collection("Products")
          .where('sellerUserId', '==', user.uid)
          .orderBy("dateCreated", "desc")
          .onSnapshot(snapshot => {
            const listItems = snapshot.docs.map(doc => ({
              id: doc.id,
              productName: doc.data().productName,
              category: doc.data().category,
              productImage: doc.data().productImage,
              price: doc.data().price,
              purchasedNum: doc.data().purchasedNum,
              sellerUserId: doc.data().sellerUserId,
            }))
            setSellerItems(listItems)
          })
      }
    }
    fetchData();
  }, []);

  return (
    <SellerProductContext.Provider
      value={{
        myProducts: [sellerItems, setSellerItems],
        productData: [productDetails, setProductDetails],
        productCategories: [categories]
      }}>
      {children}
    </SellerProductContext.Provider>
  )
}
