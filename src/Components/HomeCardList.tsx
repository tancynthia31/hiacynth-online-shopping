import React, {
    useState,
    useContext,
    FunctionComponent
  } from "react";
  import { makeStyles } from '@material-ui/core/styles';
  import {
    Card,
    CardActionArea,
    CardContent,
    CardMedia,
    Typography,
    Button,
    Grid
  } from '@material-ui/core';
  import { ProductContext } from "../Contexts/ProductContext";
  import Pagination from "./Pagination"
  import { useHistory } from "react-router-dom";
  import CircularProgress from '@material-ui/core/CircularProgress';
  
  const useStyles = makeStyles({
    root: {
      width: 280,
      marginTop: 50,
      marginLeft: 20,
      marginRight: 20,
      display: "flex",
      cursor: "pointer",
    },
    media: {
      height: 200,
      width: 280
    },
    gridItems: {
      marginTop: 50
    },
    spinner: {
        position: "fixed",
         top: "50%", 
         left: "50%", 
         color: "blue"
    }
  });
  
  const HomeCardList: FunctionComponent = () => {
      const history = useHistory();
    const classes = useStyles();
    const { filtered } = useContext(ProductContext);
    const [filteredItems] = filtered
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(8);
  
    if (filteredItems === undefined || filteredItems.length == 0){
      return (
      <CircularProgress 
      disableShrink 
    className={classes.spinner}
      />);
    }

    const goToLogin = () => {
     alert("Log in first");
     history.push('/login')

    }
    const indexOfLastPost = Number(currentPage) * Number(postsPerPage);
    const indexOfFirstPost = Number(indexOfLastPost) - Number(postsPerPage);
    const current = filteredItems.slice(indexOfFirstPost, indexOfLastPost);
    const paginate = (pageNumber: any) => {
      setCurrentPage(pageNumber);
    }
  
    return (
      <div>
           <div
           style={{
               backgroundImage:`url(https://images.theconversation.com/files/351878/original/file-20200810-22-przb4a.jpg?ixlib=rb-1.1.0&rect=0%2C0%2C5760%2C2880&q=45&auto=format&w=1356&h=668&fit=crop)`,
            height:200,
            backgroundSize:"cover",
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            marginBottom:-50,
            paddingTop:70,
            opacity:"90%"
            }} >

            <Typography variant="h3"
            style={{color:"white", textAlign:"center", textShadow:"2px 2px black", marginTop:50}}>
            Hiacynth Online Shopping</Typography>
            <Grid
                  container
                  justify="center"
                >
            <Button
            color="secondary"
            variant="contained"
            onClick={goToLogin}
            >
                Shop now
            </Button>
            </Grid>
         
        </div>
          <React.Fragment>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className={classes.gridItems}
        >
          {current.map((item: any) => (
            <Card className={classes.root} key={item.id}
              style={{ backgroundColor: '#fafafa' }}>
              <CardActionArea>
                <CardMedia
                  className={classes.media}
                  image={item.productImage}
                  title={item.category}
                />
                <CardContent>
                  <Typography gutterBottom variant="h6" >
                    {item.productName}
                  </Typography>
                  <Typography variant="button">
                    Php {item.price} .00
                  </Typography>
                  <Typography gutterBottom variant="subtitle1"
                    color="textSecondary">
                    {item.category}
                  </Typography>
                  <Typography gutterBottom variant="overline">
                    {item.purchasedNum} SOLD
                  </Typography>
                  <div>
                    <Button variant="contained" color="secondary"
                      onClick={goToLogin}>
                      BUY NOW
                    </Button>
                  </div>
                </CardContent>
              </CardActionArea>
            </Card>
          ))}
        </Grid>
    
        <Pagination
          postsPerPage={postsPerPage}
          totalPosts={filteredItems.length}
          paginate={paginate}
        />
  </React.Fragment>
      </div>
    );
  }
  export default HomeCardList;