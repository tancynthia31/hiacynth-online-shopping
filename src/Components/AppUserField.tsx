import React, { FunctionComponent, useState, useContext } from 'react'
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";
import {
  makeStyles,
  Card,
  CardContent,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button
} from '@material-ui/core';
import { UserContext } from "../Contexts/UserContext"
import { changeAddress } from "../apis/AccountApi"

const useStyles = makeStyles({
  root: {
    width: 800,
    margin: "auto",
    marginTop: 100
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});


const AppUserField: FunctionComponent = () => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [userData] = useContext(UserContext);
  const [currentAdd, setCurrentAdd] = useState(" ");


  const updateAddress = (data: any) => {
    data.preventDefault();
    changeAddress(currentAdd);
    setOpen(false);
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event: any) => {
    setCurrentAdd(event.target.value);
  }

  return (
    <div>
      <Card
        className={classes.root}>
        <CardContent>
          <h4 className="AppHeader">
            C H E C K O UT
           </h4>
          <table>
            <tbody>
              <tr>
                <td>
                  Name : {userData.firstName} {userData.lastName}
                </td>
              </tr>
              <tr>
                <td>
                  Contact Number : {userData.contact}
                </td>
              </tr>
              <tr>
                <td>
                  Complete Address : {userData.address}
                </td>
                <td>
                  <Button
                    style={{ textAlign: "right" }}
                    variant="outlined"
                    color="primary"
                    onClick={(e) => {
                      e.preventDefault();
                      setCurrentAdd(userData.address)
                      setOpen(true);
                    }}>
                    EDIT
                  </Button>
                </td>
              </tr>
            </tbody>
          </table>
        </CardContent>

      </Card>

      <Dialog open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <b>Update home address</b>
        </DialogTitle>
        <DialogContent>
          <form onSubmit={updateAddress}
            autoComplete="off"
            className="AppForm">
            <div>
              <input
                className="AppInput"
                type="text"
                name="address"
                onChange={handleChange}
                value={currentAdd}
              />
            </div>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
            </Button>
              <Button type="submit" color="primary">
                SAVE
            </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  )
}
export default AppUserField
