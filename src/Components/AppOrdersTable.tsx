import React, { useContext } from 'react'
import AppAccountNav from '../Components/AppAccountNav'
import { makeStyles } from '@material-ui/core/styles';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Card,
    CardContent
} from '@material-ui/core';

import { OrdersContext } from "../Contexts/OrdersContext"

const useStyles = makeStyles({
    table: {
        width: 900,
        margin: "auto"
    },
    card: {
        width: 1100,
        margin: "auto",
        marginTop: 100,
    }
});

const AppOrdersTable = () => {
    const classes = useStyles();
    const [orders] = useContext(OrdersContext);

    return (
        <div>
            <Card className={classes.card}>
                <CardContent>
                    <h3 className="AppHeader">T R A N S A C T I O N S</h3>
                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell><h3>ORDER NUMBER</h3></TableCell>
                                    <TableCell align="right"><h3>DATE ORDERED</h3></TableCell>
                                    <TableCell align="right"><h3>ORDERS</h3></TableCell>
                                    <TableCell align="right"><h3>TOTAL AMOUNT</h3></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {orders.map((order: any) => (
                                    <TableRow key={order.id}>
                                        <TableCell>{order.orderNumber}
                                        </TableCell>
                                        <TableCell align="right">{order.dateOrdered}</TableCell>
                                        <TableCell align="right">
                                            <h4 className="AppItem">ITEMS: </h4>
                                            {order.orders.map((eachItem: any) => {
                                                const separated = eachItem.split('/');
                                                return (
                                                    <h4 className="AppItem"> {separated[2]} - {separated[1]} pcs.
                 Amount: P {separated[3]}.00 </h4>
                                                )
                                            })}</TableCell>
                                        <TableCell align="right">P {order.totalAmount}.00</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </CardContent>
            </Card>
        </div>
    )
}

export default AppOrdersTable




