import React, { FunctionComponent, useContext } from 'react'
import "firebase/auth";
import "firebase/firestore";
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  IconButton,
  Checkbox,
  TableFooter,
  Grid,
  Typography,
  Button
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { useHistory } from "react-router-dom";
import { CartContext } from "../Contexts/CartContext"
import {
  deleteItem,
  checkItem,
  addItem,
  decreaseItem
} from "../apis/CartApi"

const useStyles = makeStyles({
  table: {
    width: 800,
    margin: "auto",
    marginTop: 100
  }
});

const AppCartTable: FunctionComponent = () => {
  const history = useHistory();
  const classes = useStyles();
  const { items, total } = useContext(CartContext);
  const [cartItems, setCartItems] = items
  const { amount } = total

  const deleteItemFromCart = (itemId: string) => {
    deleteItem(itemId);
  }

  const handleChange = (id: any, checked: any) => {
    checkItem(id, checked);
  };

  const increment = (id: any, qty: number, price: number) => {
    addItem(id, qty, price);
  };

  const decrement = (id: any, qty: number, price: number) => {
    decreaseItem(id, qty, price);
  };

  return (
    <div>
      <TableContainer className={classes.table} component={Paper}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>
                <Typography variant="button">Select</Typography>
              </TableCell>
              <TableCell>
                <Typography variant="button">Product Name</Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="button">Price</Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="button">Quantity</Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="button">Amount</Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="button">Delete</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {cartItems.map((item: any) => (
              <TableRow key={item.id}>
                <TableCell>
                  <Checkbox
                    checked={item.checked}
                    color="primary"
                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                    onChange={(e) => {
                      e.preventDefault();
                      handleChange(item.id, item.checked)
                    }} />
                </TableCell>
                <TableCell component="th" scope="row">
                  {item.productName}
                </TableCell>
                <TableCell align="right">
                  {item.price}
                </TableCell>
                <TableCell align="right">
                  <button
                    onClick={(e) => {
                      e.preventDefault();
                      decrement(item.id, item.quantity, item.price)
                    }}>-</button>
                     &nbsp;&nbsp;{item.quantity} &nbsp;&nbsp;
                <button
                    onClick={(e) => {
                      e.preventDefault();
                      increment(item.id, item.quantity, item.price)
                    }}>+</button>
                </TableCell>
                <TableCell align="right">
                  {item.amount}
                </TableCell>
                <TableCell align="right">
                  <IconButton aria-label="delete"
                    onClick={() => deleteItemFromCart(item.id)}>
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell colSpan={6}>
                <Typography variant="h6"
                  style={{ textAlign: "right" }}>
                  TOTAL AMOUNT : P {amount} .00
              </Typography>
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Button style={{ marginTop: 50, marginBottom: 50 }} color="secondary" variant="contained"
          onClick={() => history.push('/checkout')}>
          CHECKOUT
        </Button>
      </Grid>
    </div>
  )
}

export default AppCartTable