import React, {
  useState,
  useContext,
  FunctionComponent
} from "react";
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Grid
} from '@material-ui/core';
import { ProductContext } from "../Contexts/ProductContext";
import Pagination from "./Pagination"
import { addToCart } from '../apis/ProductApi';

const useStyles = makeStyles({
  root: {
    width: 280,
    marginTop: 50,
    marginLeft: 20,
    marginRight: 20,
    display: "flex",
    cursor: "pointer",
  },
  media: {
    height: 200,
    width: 280
  },
  gridItems: {
    marginTop: 50
  }
});

const AppCardList: FunctionComponent = () => {
  const classes = useStyles();
  const db = firebase.firestore();
  const user = firebase.auth().currentUser;
  const [qty, setQty] = useState(1);
  const [productId, setProductId] = useState('');
  const [open, setOpen] = useState(false);
  const { filtered } = useContext(ProductContext);
  const [filteredItems] = filtered
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(8);
  const [productDetails, setProductDetails] = useState<any>([]);

  const handleOpen = (id: any) => {
    setOpen(true);
    setProductId(id);
    if (user) {
      db.collection("Products").doc(id)
        .onSnapshot(snapshot => {
          setProductDetails(snapshot.data())
        }, (error) => {
          alert(error.message);
        });
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event: any) => {
    setQty(event.target.value);
  }

  const handleAddToCart = (data: any) => {
    data.preventDefault();
    addToCart(productDetails, productId, qty)
    setOpen(false);
    setQty(1);
  }
  const indexOfLastPost = Number(currentPage) * Number(postsPerPage);
  const indexOfFirstPost = Number(indexOfLastPost) - Number(postsPerPage);
  const current = filteredItems.slice(indexOfFirstPost, indexOfLastPost);
  const paginate = (pageNumber: any) => {
    setCurrentPage(pageNumber);
  }

  return (
    <div>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        className={classes.gridItems}
      >
        {current.map((item: any) => (
          <Card className={classes.root} key={item.id}
            style={{ backgroundColor: '#fafafa' }}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image={item.productImage}
                title={item.category}
              />
              <CardContent>
                <Typography gutterBottom variant="h6" >
                  {item.productName}
                </Typography>
                <Typography variant="button">
                  Php {item.price} .00
                </Typography>
                <Typography gutterBottom variant="subtitle1"
                  color="textSecondary">
                  {item.category}
                </Typography>
                <Typography gutterBottom variant="overline">
                  {item.purchasedNum} SOLD
                </Typography>
                <div>
                  <Button variant="outlined" color="secondary"
                    onClick={() => handleOpen(item.id)}>
                    ADD TO CART
                  </Button>
                </div>
              </CardContent>
            </CardActionArea>
          </Card>
        ))}
      </Grid>

      <Pagination
        postsPerPage={postsPerPage}
        totalPosts={filteredItems.length}
        paginate={paginate}
      />

      <Dialog open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
          <b>ENTER QUANTITY</b>
        </DialogTitle>
        <DialogContent>
          <form
            onSubmit={handleAddToCart}
            autoComplete="off"
            className="AppForm">
            <input
              className="AppInput"
              type="number"
              name="quantity"
              placeholder="Enter quantity"
              onChange={handleChange}
              value={qty}
            />
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
            </Button>
              <Button type="submit" color="primary">
                ADD TO CART
            </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}
export default AppCardList;