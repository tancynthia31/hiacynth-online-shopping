import React from 'react';
import {
  Grid,
  Button
} from '@material-ui/core';

const Pagination = ({ postsPerPage, totalPosts, paginate }: any) => {
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        {pageNumbers.map((number: any) => (
          <Button style={{ marginTop: 50, marginLeft: 10, marginBottom: 100 }}
            variant="outlined" color="primary" key={number} onClick={() => paginate(number)}>
            {number}
          </Button>
        ))}
      </Grid>
    </div>

  );
};

export default Pagination;