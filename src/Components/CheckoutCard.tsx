import React, { FunctionComponent, useContext, useState } from 'react'
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableFooter,
  Paper,
  Typography,
  Grid,
  Button
} from '@material-ui/core';
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";
import { CartContext } from "../Contexts/CartContext"
import { UserContext } from "../Contexts/UserContext"
import { useHistory } from "react-router-dom";
import { orderItems } from "../apis/CartApi"


const useStyles = makeStyles({
  table: {
    width: 800,
    margin: "auto",
    marginTop: 17
  }
});

const CheckoutCard: FunctionComponent = () => {
  const history = useHistory();
  const classes = useStyles();
  const { total, selected, groupItems } = useContext(CartContext);
  const { amount } = total
  const [selectedItems] = selected
  const [group] = groupItems
  const [userData] = useContext(UserContext);

  const placeOrder = (e: any) => {
    e.preventDefault();
    orderItems(selectedItems, userData, amount)
    history.push('/account');
  }

  return (
    <div>
      <TableContainer className={classes.table}
        component={Paper}
      >
        <Table
          className={classes.table}
          size="small"
          aria-label="a dense table"
        >
          <TableHead>
            <TableRow>
              <TableCell>
                PRODUCT NAME
                </TableCell>
              <TableCell align="right">
                QUANTITY
                </TableCell>
              <TableCell align="right">
                PRICE
                </TableCell>
              <TableCell align="right">
                AMOUNT
                </TableCell>
            </TableRow>
          </TableHead>

          {Object.keys(group).map(key => (
            <>
              <TableRow>
                <TableCell
                  className="AppHighLight"
                  colSpan={4}>
                  Seller: {key}
                </TableCell>
              </TableRow>

              {Object.keys(group[key]).map(item => (
                <TableBody key={key}>
                  <TableRow key={item}>
                    <TableCell
                      component="th"
                      scope="row">
                      {group[key][item].items['productName']}
                    </TableCell>
                    <TableCell
                      align="right">
                      {group[key][item].items['quantity']}
                    </TableCell>
                    <TableCell
                      align="right">
                      {group[key][item].items['price']}
                    </TableCell>
                    <TableCell
                      align="right">
                      {group[key][item].items['amount']}
                    </TableCell>
                  </TableRow>
                </TableBody>
              ))}
            </>
          ))}
          <TableFooter>
            <TableRow>
              <TableCell colSpan={4}>
                <Typography
                  style={{ textAlign: "right" }}
                  variant='h6'>
                  TOTAL AMOUNT: P {amount} .00
              </Typography>
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center">
        <Button
          style={{ marginTop: 50, marginBottom: 50 }}
          color="secondary"
          variant="contained"
          onClick={placeOrder}>
          PLACE ORDER
        </Button>
      </Grid>
    </div>
  )
}
export default CheckoutCard
