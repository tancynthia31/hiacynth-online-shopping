import React, { FunctionComponent, useContext, useState } from 'react';
import { useForm } from 'react-hook-form';
import "firebase/auth";
import "firebase/firestore";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { UserContext } from "../Contexts/UserContext"
import { SellerProductContext } from '../Contexts/SellerProductContext'
import { addItem } from "../apis/ProductApi"

const AddProductModal: FunctionComponent = () => {
    const [userData] = useContext(UserContext);
    const { register, handleSubmit, errors } = useForm();
    const { productCategories } = useContext(SellerProductContext);
    const [categories] = productCategories
    const [open, setOpen] = useState(false);

    const onSubmit = (data: any) => {
        addItem(data, userData)
        handleClose();
    }

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    return (
        <div>
            <Button
                style={{ right: 100, top: 100, position: "fixed" }}
                variant="contained"
                color="secondary"
                onClick={handleOpen}>
                <AddCircleIcon />
        ADD ITEM
        </Button>
            <Dialog open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">
                    <b>ADD PRODUCT</b>
                </DialogTitle>
                <DialogContent>
                    <form
                        onSubmit={handleSubmit(onSubmit)}
                        autoComplete="off"
                        className="AppForm">
                        <div>
                            <input
                                className="AppInput"
                                type="text"
                                name="productName"
                                placeholder="Product name"
                                ref={register({ required: true })}
                            />
                        </div>
                        {errors.productName && <div className="AppErrorMessage">
                            This field is required</div>}
                        <div>
                            <input
                                className="AppInput"
                                type="text"
                                name="productImage"
                                placeholder="Paste image url"
                                ref={register({ required: true })}
                            />
                        </div>
                        {errors.productImage && <div className="AppErrorMessage">
                            This field is required </div>}
                        <div>
                            <input
                                className="AppInput"
                                type="number"
                                name="price"
                                placeholder="Price in php"
                                ref={register({ required: true })}
                            />
                        </div>
                        {errors.price && <div className="AppErrorMessage">
                            This field is required </div>}
                        <div>
                            <select name="category"
                                className="AppSelect"
                                ref={register({ required: true })} >
                                {categories.map((value: any) => (
                                    <option key={value} value={value}>
                                        {value}
                                    </option>
                                ))}
                            </select>
                        </div>
                        {errors.category && <div className="AppErrorMessage">
                            This field is required</div>}
                        <DialogActions>
                            <Button onClick={handleClose} color="primary">
                                Cancel
                    </Button>
                            <Button onClick={handleSubmit(onSubmit)} color="primary">
                                ADD
                    </Button>
                        </DialogActions>
                    </form>
                </DialogContent>
            </Dialog>
        </div>
    );
}
export default AddProductModal

