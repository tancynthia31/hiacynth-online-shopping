import React, { useState, useContext } from "react";
import firebase from "../firebase";
import "firebase/auth";
import "firebase/firestore";
import {
  makeStyles,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle
} from '@material-ui/core';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { SellerProductContext } from '../Contexts/SellerProductContext'
import {
  editProduct,
  deleteItem
} from "../apis/ProductApi"

const useStyles = makeStyles({
  root: {
    maxWidth: 500,
    marginTop: 20,
    margin: "auto",
    height: 340
  },
  media: {
    height: 150,
  },
  title: {
    color: "black",
  }
});

const AppSellerCardList = () => {
  const classes = useStyles();
  const user = firebase.auth().currentUser;
  const db = firebase.firestore();
  const {
    myProducts,
    productData,
    productCategories
  } = useContext(SellerProductContext);
  const [sellerItems] = myProducts
  const [productDetails, setProductDetails] = productData
  const [categories] = productCategories
  const [open, setOpen] = useState(false);
  const [productId, setProductId] = useState('');

  const deleteProduct = (productId: string) => {
    confirmAlert({
      title: 'Confirm to delete',
      message: 'This will permanently delete the item.',
      buttons: [
        {
          label: 'Cancel',
          onClick: () => handleClose()
        },
        {
          label: 'Delete',
          onClick: () => {
            deleteItem(productId, productDetails)
          }
        }
      ]
    });
  }

  const updateState = (id: string) => {
    setProductId(id);
    setOpen(true);
    if (user) {
      db.collection("Products").doc(id)
        .get().then(snapshot => {
          setProductDetails(snapshot.data())
        }, (error) => {
          alert(error.message);
        });
    }
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event: any) => {
    var { name, value } = event.target;
    setProductDetails({
      ...productDetails,
      [name]: value
    })

  }
  const onSubmit = (data: any) => {
    data.preventDefault();
    editProduct(productId, productDetails);
    handleClose();
  }

  return (
    <div style={{marginTop:100, marginBottom:100}}>
      {sellerItems.map((item: any) => (
        <div key={item.id}>
          <Card
            className={classes.root}
            style={{ backgroundColor: '#fafafa' }}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image={item.productImage}
                title={item.category}
              />
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h6">
                  Product Name: {item.productName}
                </Typography>
                <Typography
                  variant="h6"
                  color="textSecondary">
                  Product Price: Php {item.price} .00
               </Typography>
                <Typography
                  gutterBottom
                  variant="button">
                  Category: {item.category}
                </Typography>
                <Typography
                  gutterBottom
                  variant="h6">
                  {item.purchasedNum} SOLD
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions style={{ marginTop: -30, float: "right" }}>
              <Button
                variant="outlined"
                color="primary"
                onClick={() => updateState(item.id)}>
                EDIT
              </Button>
              <Button
                variant="outlined"
                type="submit"
                color="secondary"
                onClick={() => deleteProduct(item.id)}>
                DELETE
              </Button>
            </CardActions>
          </Card>
        </div>
      ))}

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title">
        <DialogTitle
          id="form-dialog-title">
          <b>Edit Product</b>
        </DialogTitle>
        <DialogContent>
          <div>
            <input
              className="AppInput"
              type="text"
              name="productName"
              placeholder="Product name"
              value={productDetails.productName}
              onChange={handleChange}
            />
          </div>
          <div>
            <input
              className="AppInput"
              type="text"
              name="productImage"
              placeholder="Paste image url"
              value={productDetails.productImage}
              onChange={handleChange}
            />
          </div>
          <div>
            <input
              className="AppInput"
              type="number"
              name="price"
              placeholder="Price in php"
              value={productDetails.price}
              onChange={handleChange}
            />
          </div>
          <div>
            <select name="category"
              className="AppSelect"
              onChange={handleChange}
              value={productDetails.category}
            >
              {categories.map((value: any) => (
                <option key={value} value={value}>
                  {value}
                </option>
              ))}
            </select>
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            color="primary">
            Cancel
          </Button>
          <Button
            onClick={onSubmit}
            color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default AppSellerCardList;