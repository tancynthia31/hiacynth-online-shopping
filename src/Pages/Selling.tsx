import React, { FunctionComponent } from 'react'
import AppAccountNav from '../Components/AppAccountNav'
import AppModal from '../Components/AddProductModal'
import AppProductList from '../Components/AppSellerCardList'
import firebase from "../firebase";
import { Redirect } from "react-router";

const Selling: FunctionComponent = () => {
    var currentUser = firebase.auth().currentUser;

    if (!!!currentUser) {
        return <Redirect to="/not-found-page" />;
    }
    return (
        <div>
            <AppAccountNav />
            <AppModal />
            <AppProductList />
        </div>
    )
}
export default Selling
