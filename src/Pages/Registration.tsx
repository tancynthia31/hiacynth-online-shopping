import React, { FunctionComponent } from 'react'
import { useForm } from 'react-hook-form';
import '../App.css';
import AppNavBar from '../Components/AppNavBar'
import {
    Grid,
    Paper,
    Typography
} from '@material-ui/core';
import { useHistory } from "react-router-dom"

import { registerAccount } from '../apis/AccountApi'

const Registration: FunctionComponent = () => {
    const { register, handleSubmit, errors } = useForm();
    const history = useHistory()
    const onSubmit = (data: any) => {
        registerAccount(data);
        history.push('/login')
    }

    return (
        <div>
            <AppNavBar />
            <Paper className="AppPaper"
                style={{ backgroundColor: "#fafafa", marginTop:100 }}
            >
                <Grid container >
                    <form
                        onSubmit={handleSubmit(onSubmit)}
                        autoComplete="off"
                        className="AppForm">
                        <Typography variant="h5">
                            SIGN UP HERE
                        </Typography>
                        <div>
                            <input
                                className="AppInput"
                                type="text"
                                name="firstName"
                                placeholder="First name"
                                ref={register({ required: true })}
                            />
                            {errors.firstName && <div className="AppErrorMessage">
                                This field is required</div>}
                        </div>
                        <div>
                            <input
                                className="AppInput"
                                type="text"
                                name="lastName"
                                placeholder="Last name"
                                ref={register({ required: true })}
                            />
                            {errors.lastName && <div className="AppErrorMessage">
                                This field is required </div>}
                        </div>
                        <div>
                            <input
                                className="AppInput"
                                type="email"
                                name="email"
                                placeholder="Email address"
                                ref={register({
                                    required: true,
                                    pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
                                })}
                            />
                            {errors.email && <div className="AppErrorMessage">
                                Invalid email address</div>}
                        </div>
                        <div>
                            <input
                                className="AppInput"
                                type="number"
                                name="contact"
                                placeholder="Phone number"
                                ref={register({ required: true, maxLength: 11, minLength: 11 })}
                            />
                            {errors.contact && <div className="AppErrorMessage">
                                Invalid phone number</div>}
                        </div>
                        <div>
                            <input
                                className="AppInput"
                                type="text"
                                name="address"
                                placeholder="Address"
                                ref={register({ required: true })}
                            />
                            {errors.address && <div className="AppErrorMessage">
                                This field is required </div>}
                        </div>
                        <div>
                            <input
                                className="AppInput"
                                type="password"
                                name="password"
                                placeholder="Password"
                                ref={register({ required: true })}
                            />
                            {errors.password && <div className="AppErrorMessage">
                                This field is required </div>}
                        </div>
                        <div>
                            <input
                                className="AppSubmitBtn"
                                type="submit"
                                autoComplete="current-password"
                                value="REGISTER" />
                        </div>
                    </form>
                </Grid>
            </Paper>
        </div>
    )
}
export default Registration

