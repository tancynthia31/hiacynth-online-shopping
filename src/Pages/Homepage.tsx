import React, {FunctionComponent} from 'react'
import AppNavBar from '../Components/AppNavBar';
import HomeCardList from '../Components/HomeCardList';

const Homepage: FunctionComponent = () => {
    return (
        <div>
    <AppNavBar />
    <HomeCardList />
        </div>
    )
}

export default Homepage
