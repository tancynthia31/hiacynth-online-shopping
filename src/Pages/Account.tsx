import React, { FunctionComponent, useContext } from 'react'
import AppAccountNav from '../Components/AppAccountNav'
import firebase from "../firebase";
import { Redirect } from "react-router";
import AppCardList from '../Components/AppCardList'

const Account: FunctionComponent = () => {
    var currentUser = firebase.auth().currentUser;
    if (!!!currentUser) {
        return <Redirect to="/not-found-page" />;
    }
    return (
        <div>
            <AppAccountNav />
            <AppCardList />
        </div>
    )
}

export default Account
