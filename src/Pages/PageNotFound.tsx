import React, { FunctionComponent } from 'react'
import { useHistory } from "react-router-dom";


const PageNotFound: FunctionComponent = () => {
    const history = useHistory();
    const onBack = () => {
        history.push("/");
    }
    return (
        <div>
            <h1>Page not found. Please log in.</h1>
            <button onClick={onBack}>
                BACK HOME
            </button>
        </div>
    )
}

export default PageNotFound