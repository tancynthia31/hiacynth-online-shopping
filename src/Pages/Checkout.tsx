import React, { FunctionComponent } from 'react'
import AppAccountNav from '../Components/AppAccountNav'
import { Redirect } from "react-router";
import firebase from "../firebase";
import AppUserField from "../Components/AppUserField"
import CheckoutCard from "../Components/CheckoutCard"

const Cart: FunctionComponent = () => {
    var currentUser = firebase.auth().currentUser;
    if (!!!currentUser) {
        return <Redirect to="/not-found-page" />;
    }
    return (
        <div>
            <AppAccountNav />
            <AppUserField />
            <CheckoutCard />
        </div>
    )
}

export default Cart
