import React, { FunctionComponent } from "react";
import { useForm } from 'react-hook-form';
import AppNavBar from '../Components/AppNavBar';
import {
  Grid,
  Paper,
  Typography
} from '@material-ui/core';
import { signIn } from '../apis/AccountApi';
import { useHistory } from "react-router-dom";

const Login: FunctionComponent = () => {
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = (data: any) => {
    signIn(data.email, data.password);
  }

  return (
    <div>
      <AppNavBar />
      <Paper className="AppPaper" style={{marginTop:100,backgroundColor:"#fafafa"}}>
        <Grid container >
          <form
            onSubmit={handleSubmit(onSubmit)}
            noValidate autoComplete="off"
            className="AppForm">
            <Typography variant="h5">LOG IN</Typography>
            <div>
              <input
                className="AppInput"
                type="email"
                name="email"
                placeholder="Email"
                ref={register({ required: true })}
              />
              {errors.email && <div className="AppErrorMessage">
                This field is required </div>}
            </div>
            <div>
              <input
                className="AppInput"
                type="password"
                name="password"
                placeholder="Password"
                ref={register({ required: true })}
              />
              {errors.password && <div className="AppErrorMessage">
                This field is required </div>}
            </div>
            <div>
              <input
                className="AppSubmitBtn"
                type="submit"
                value="SIGN IN" />
            </div>
          </form>
        </Grid>
        <Grid
        container
        direction="row"
        justify="center"
        alignItems="center">
    
          <Typography
           style={{marginTop:30}} 
          >
            No account? &nbsp;
            <span 
            style={{cursor:"pointer", textDecoration:"underline"}} 
            onClick={() => {history.push('/signup')}}>
               Register Here
            </span>
          </Typography>
        </Grid>
      </Paper>
    </div>
  );
}

export default Login