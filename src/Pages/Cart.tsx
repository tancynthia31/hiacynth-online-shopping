import React, { FunctionComponent } from 'react'
import AppAccountNav from '../Components/AppAccountNav'
import { Redirect } from "react-router";
import firebase from "../firebase";
import AppCartTable from '../Components/AppCartTable'

const Cart: FunctionComponent = () => {
    var currentUser = firebase.auth().currentUser;
    if (!!!currentUser) {
        return <Redirect to="/not-found-page" />;
    }
    return (
        <div>
            <AppAccountNav />
            <AppCartTable />
        </div>
    )
}

export default Cart
