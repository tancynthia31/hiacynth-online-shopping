import React from 'react'
import firebase from "../firebase";
import AppAccountNav from '../Components/AppAccountNav'
import AppAOrdersTable from '../Components/AppOrdersTable'
import { Redirect } from "react-router";
import AppOrdersTable from '../Components/AppOrdersTable';


const Transactions = () => {
  var currentUser = firebase.auth().currentUser;
  if (!!!currentUser) {
    return <Redirect to="/not-found-page" />;
  }
  return (
    <div>
      <AppAccountNav />
      <AppOrdersTable />
    </div>
  )
}

export default Transactions




